import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'
import userRouter from './src/routes/userRoutes'
import { badRequestHandler } from './src/middlewares/errorHandling'
import handleErrors from './src/middlewares/errorHandling'
connect(
  'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority',
  { useNewUrlParser: true }
)

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.use('/users', userRouter)

//If express controller middleware fails, next middleware will be error handling middleware
handleErrors(app)

app.listen(8080, () => console.log('Example app listening on port 8080!'))
