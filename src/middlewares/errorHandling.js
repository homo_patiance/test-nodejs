/***
 * 1- This file keeps basic error handling middlewares
 *
 * 2- If any routes fails and goes to next middleware one of error handling middleware handles
 *    error according to it's error code
 *
 * 3- Generic error middleware should be at the bottom (err code:500). It works if the other middlewares
 *    skip the error
 */

const badRequestHandler = (err, req, res, next) => {
  if (err.httpStatusCode === 400) {
    return res.status(400).json({ errors: err.message || 'Bad Request!' })
  }
  next(err)
}

const notFoundHandler = (err, req, res, next) => {
  if (err.httpStatusCode === 404) {
    return res.status(404).json({
      errors: err.message ? err.message + ' ' + 'Not Found!' : 'Not Found!',
    })
  }
  next(err)
}

const unAuthorizedHandler = (err, req, res, next) => {
  if (err.httpStatusCode === 401) {
    return res.status(401).json({ errors: err.message || 'Unauthorized!' })
  }
  next(err)
}

const forbiddenHandler = (err, req, res, next) => {
  if (err.httpStatusCode === 403) {
    return res.status(403).json({ errors: err.message || 'Forbidden!' })
  }
  next(err)
}

const genericHandler = (err, req, res, next) => {
  if (!res.headersSent) {
    return res.status(err.httpStatusCode || 500).json({ errors: err.message || 'Internal Server Error' })
  }
}

//generic error should be last one
const handleErrors = (app) => {
  app.use(badRequestHandler)
  app.use(forbiddenHandler)
  app.use(unAuthorizedHandler)
  app.use(notFoundHandler)
  app.use(genericHandler)
}
export default handleErrors
