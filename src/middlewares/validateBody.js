/**
 * 1- This file keeps schemas that needs to be validated and validate body function
 * Steps to validate a field:
 * 1- Create a Joi schema for body
 * 2- Call "validateBody" function as an express middleware
 * 3- Pass Joi Schema to validateBody
 */

import Joi from 'joi'

export const userValidation = Joi.object().keys({
  firstName: Joi.string().min(1).required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required(),
})

/**
 * Takes joi schema and edits error message from joi response and sends it as a response with 400 (bad request) code
 * @param {Joi Schema} schema joi schema
 */
export const validateBody = (schema) => {
  return (req, res, next) => {
    const { error } = schema.validate(req.body)

    if (error) {
      let originalErrorMessage = error.details[0].message
      let modifiedErrorMessage =
        error.details[0].path + ' ' + originalErrorMessage.substring(originalErrorMessage.indexOf(' ') + 1)
      return res.status(400).send({ error: modifiedErrorMessage })
    }

    if (!req.value) {
      req.value = {}
    }

    next()
  }
}
