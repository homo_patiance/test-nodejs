/**
 * Takes express middleware and operates try-catch block.
 * With using this, we no longer need to add try-catch block for each request controller
 * @param handler express middleware
 */
const tryCatchWrapper = (handler) => {
  return async (req, res, next) => {
    try {
      await handler(req, res)
    } catch (error) {
      next(error)
    }
  }
}

export default tryCatchWrapper
