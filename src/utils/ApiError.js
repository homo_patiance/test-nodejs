class ApiError extends Error {
  constructor(httpStatusCode = 500, message) {
    super()
    this.httpStatusCode = httpStatusCode
    this.message = message
  }
}

export default ApiError
