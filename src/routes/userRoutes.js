/**
 * This file handles user-related routes
 * Each middleware wrapped into "tryCatchWrapper" which handles try-catch block for each middleware
 */

import { Router } from 'express'
import { addNewUser, getAllUsers, getUserByIdAndDelete, getUserByIdAndUpdate } from '../controllers/userControllers'
import tryCatchWrapper from '../utils/tryCatchWrapper'
import { userValidation, validateBody } from '../middlewares/validateBody'

const userRouter = Router()

userRouter.get('/', tryCatchWrapper(getAllUsers))

userRouter.post('/', validateBody(userValidation), tryCatchWrapper(addNewUser))
userRouter.put('/:userId', validateBody(userValidation), tryCatchWrapper(getUserByIdAndUpdate))
userRouter.delete('/:userId', tryCatchWrapper(getUserByIdAndUpdate), tryCatchWrapper(getUserByIdAndDelete))

export default userRouter
