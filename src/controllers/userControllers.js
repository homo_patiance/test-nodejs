/**
 * !!! ASYNC functions didn't wrapped inside "try-catch" block. They already in routes folder (where they called)
 *
 * This file handles user-related bussiness logic operations,
 *
 * Middleware doesn't need a "next" middleware. It's already handled by tryCatchWrapper
 */

import UserModel from '../models/UserModel'
import ApiError from '../utils/ApiError'

export const getAllUsers = async (req, res) => {
  const allUsers = await UserModel.find({})
  res.status(200).send(allUsers)
}

/**
 *
 * Creates new user
 * If user email exist throws an Error
 */
export const addNewUser = async (req, res) => {
  const { email } = req.body
  const foundUser = await UserModel.findOne({ email })
  if (foundUser) throw new ApiError(400, 'Email already exist!')

  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  const newUser = await user.save() //returns new user if everything goes well, otherwise goes to catch block
  if (!newUser) throw new ApiError(500, 'Internal Server Error')
  res.send(newUser)
}

/**
 * Takes userId from express req.params and updates user
 * Body validation handled by a middleware, after finding user, new data directly updated
 */
export const getUserByIdAndUpdate = async (req, res) => {
  const { userId } = req.params

  const updatedUser = await UserModel.findByIdAndUpdate(userId, req.body, { new: true })

  if (!updatedUser) throw new ApiError(404, 'User not found!')

  res.status(200).send(updatedUser)
}
export const getUserByIdAndDelete = async (req, res) => {
  const { userId } = req.params
  const deletedUser = await UserModel.findByIdAndDelete(userId)
  if (!deletedUser) throw new ApiError(404, 'User not found!')
  res.status(200).send('OK')
}
